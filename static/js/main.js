/*
 * The main Lory JavaScript code.
 * Copyright 2013 Thomas Klemz
 */

$(document).ready(function() {
  
  function didRetr(mail) {
    console.log("retreived some mail");
    
    var maillist = document.getElementById("maillist");
    var letters = document.createDocumentFragment();
    
    //mail.bag.sort(naturalSort);
    
    doneChunking = !mail.bag.length;
    
    for (var i = 0, len = mail.bag.length; i < len; ++i) {
      var mailitem = document.createElement("li");
      mailitem.className = "mailitem";
      mailitem.appendChild(document.createTextNode(mail.bag[i]));
      letters.appendChild(mailitem);
    }
    maillist.appendChild(letters);
  }
  
  function errRetr(err) {
    console.log("error retrieving mail:");
    console.log(err);
  }
  
  $("#searchbar").on("keydown", function(e) {
    if(e.which == 13) {
        $("#maillist").empty();
        goSearch($("#search").val());
    }
  });
  
  var reqChunk = 1;
  var doneChunking = false;
  
  $(window).scroll(function() {
    console.log($(window).scrollTop() + ' ' + $("#maillist").height());
    var scrollAmt = $(window).scrollTop();
    var maillistHeight = $("#maillist").height();
    
    if(scrollAmt >= 0.7*maillistHeight) {
      if(!doneChunking) {
        ++reqChunk; 
        goSearch();
        doneChunking = true;
      }
    }
  });
  
  function goSearch(input) {
    var filter = {};
    filter["Command"] = "";
    filter["Chunk"] = reqChunk;
    //TODO: parse input
    var terms = input ? input.split(" ") : [];

    filter["Terms"] = terms;
    
    $.ajax({
      type: "POST",
      url: "/retr",
      dataType: "json",
      data: JSON.stringify(filter),
      success: didRetr,
      error: errRetr
    });
  }
  
  goSearch();
});
