package user

import (
  "fmt"
  "net/mail"
  "path/filepath"
  "os"
  "text/scanner"
  "encoding/csv"
  "unicode"
  "strings"
  "utils"
  "email"
  "bytes"
  "strconv"
)
//TODO: Id -> int64
type User struct {
  EmailId int
  Emails map[int]*email.Email
  AllSortedEmailList []int
  UsedWords utils.SearchMap
  Id int
  Name string
  LoryName string
}

const usersDir = ".users"
const userListFile = usersDir + "/.userlist"

var UserMap = make(map[int]*User)
var userId int

func FindUser(loryname string) *User {
  for _,user := range(UserMap) {
    if user.LoryName == loryname {
      return user
    }
  }
  return nil
}

func (user *User) AddEmail(path string) *email.Email {
  user.EmailId++
  email := email.Email{Id: user.EmailId, Path: path}
  user.Emails[email.Id] = &email
  return &email
}

func (user *User) addUsedWord(str* string, emailId int) {
    if _,b := user.UsedWords[*str]; !b {
      user.UsedWords[*str] = make(utils.IndexSet)
    }
    user.UsedWords[*str][emailId] = true
}

//NOTE: searches must be all lowercase
func (user *User) SearchMail(searches []string) utils.IndexSet {
  results := user.UsedWords[searches[0]]
  
  for i := 1; i < len(searches); i++ {
    results = utils.And(results, user.UsedWords[searches[i]])
  }
  return results
}

func (user *User) getHomeDir() string {
  return fmt.Sprintf("./%s/%s", usersDir, user.LoryName) 
}

func (user *User) getDumpDir() string {
  return fmt.Sprintf("%s/.dump", user.getHomeDir()) 
}

func makeDir(name string) {
  os.Mkdir(name, 0700)
}

func (user *User) makeDirs() {
  makeDir(user.getHomeDir())
  makeDir(user.getDumpDir())
}

func (user *User) getDumpFile(filename string) string {
  return fmt.Sprintf("%s/%s", user.getDumpDir(), filename)
}

func (user *User) getSearchMapFile() string {
  return user.getDumpFile("searchMap")
}

func (user *User) getSortedEmailListFile() string {
  return user.getDumpFile("sortedMails")
}

func CreateNewUser(loryName, name string) *User {
  userId++
  newUser := NewUser(userId, loryName, name)
  newUser.makeDirs()
  return newUser
}

func NewUser(id int, loryName, name string) *User {
  user := User{ Name: name, Id: id, LoryName: loryName }
  
  user.Emails = make(map[int]*email.Email)
  user.UsedWords = make(utils.SearchMap)
 
  addToUserMap(&user)
  
  return &user
}

func addToUserMap(user *User) {
  UserMap[user.Id] = user
  user.checkMail()
}

//might want to use a different scanner cuz this one is for go code
func (user *User) indexMessage(msg *mail.Message, emailId int) {
  var scn scanner.Scanner
  //TODO: Subject can be empty, if so simply skip subject
  scn.Init(strings.NewReader(msg.Header.Get("Subject")))
  
  //breaking up the words
  for tok := scn.Scan(); tok != scanner.EOF; tok = scn.Scan() {
    if !unicode.IsPunct(tok) {
      word := strings.ToLower(scn.TokenText())
      user.addUsedWord(&word, emailId)
    }
  }
}

func (user *User) saveSearchMap() {
  f, _ := os.OpenFile(user.getSearchMapFile(), os.O_CREATE|os.O_WRONLY, 0600)
  defer f.Close()
  
  b := new(bytes.Buffer)
  
  for key, _ := range(user.UsedWords) {
    b.WriteString(fmt.Sprintf("%s ", key))
    for key, _ := range(user.UsedWords[key]) {
      b.WriteString(fmt.Sprintf("%d ", key))
    }
    b.WriteString("\n")
  }
  
  _, err := f.Write(b.Bytes())
  if(err != nil){
    panic(err)
  }
}

func (user *User) saveSortedEmailList() {
  f, _ := os.OpenFile(user.getSortedEmailListFile(), os.O_CREATE|os.O_WRONLY, 0600)
  defer f.Close() 
  
  b := new(bytes.Buffer)

  b.WriteString(fmt.Sprintf("%d\n", user.EmailId))
  for i := range(user.AllSortedEmailList) {
    b.WriteString(fmt.Sprintf("%d ", user.AllSortedEmailList[i]))
  }
  
  _, err := f.Write(b.Bytes())
  if(err != nil){
    panic(err)
  }
}

func (user *User) saveMailInfo() {
  user.saveSearchMap()
  user.saveSortedEmailList()
}

func (user *User) sortMail() {
  user.AllSortedEmailList = email.SortMail(user.Emails)
}

func (user *User) indexMail(paths []string) {
  //msgs and paths should be the same size. If not then we are screwed
  for i := range(paths) {
    path := paths[i]
    msg, _ := utils.OpenLetter(path)
    email := user.AddEmail(path)
    user.indexMessage(msg, email.Id)
  }
}
func (user *User) getMail() {
  files, _ := filepath.Glob(fmt.Sprintf("%s/*.txt", user.getHomeDir()))
  user.indexMail(files)
}

func (user *User) checkMail() {
  user.getMail()
  user.sortMail()
  user.saveMailInfo()
  fmt.Println("done indexing user", user.Name)
}

//User file:
//345
//123,tjklemz,Thomas Klemz

func parseId(id string) int {
  realId, _ := strconv.ParseInt(id, 10, 0)
  return int(realId)
}

func readUserList() {
  f, _ := os.OpenFile(userListFile, os.O_RDONLY, 0600)
  defer f.Close()
  
  csvReader := csv.NewReader(f)
  userData, _ := csvReader.ReadAll()
  
  userId = parseId(userData[0][0])
  for i := 1; i < len(userData); i++ {
    NewUser(parseId(userData[i][0]), userData[i][1], userData[i][2])
  }
}

func init() {
  readUserList()
}
