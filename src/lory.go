//DON'T DO C#

package main

import (
  "fmt"
  "io/ioutil"
	"net/http"
	"html/template"
  "encoding/json"
  "utils"
  "user"
  "github.com/gorilla/sessions"
)

//-------- Types --------

type Filter struct {
  Command string
  Terms []string
  Chunk int
}

//-----------------------

var store = sessions.NewCookieStore([]byte("monkeys-like-kitkats"))

//var user1 = user.NewUser("tjklemz", "Thomas Klemz")
//var user2 = user.NewUser("pfinn", "Peter Finn")

func mailroom(w http.ResponseWriter, r *http.Request) {
  session, _ := store.Get(r, "lorymail")
  if session.Values["loryname"] == nil {
    http.Redirect(w, r, "/login", http.StatusSeeOther)
  }
    
  t, _ := template.ParseFiles("./templates/main.html")
  var data interface {}
  t.Execute(w, data)
}

func login(w http.ResponseWriter, r *http.Request) {
  if r.Method == "POST" {
    r.ParseForm()
    loryname := r.FormValue("username")
    _ = r.FormValue("password")
    
    user := user.FindUser(loryname)
    
    if user != nil {
      session, _ := store.Get(r, "lorymail")
      session.Options = &sessions.Options{
        Path: "/",
      }
      session.Values["loryname"] = loryname
      session.Values["uid"] = user.Id
      session.Save(r, w)
      http.Redirect(w, r, "/", http.StatusSeeOther)
    } else {
      http.Redirect(w, r, "/login", http.StatusSeeOther)
    }
  } else {
    session, _ := store.Get(r, "lorymail")
    if session.Values["loryname"] == nil {
      t, _ := template.ParseFiles("./templates/login.html")
      var data interface {}
      t.Execute(w, data)
    } else {
      http.Redirect(w, r, "/", http.StatusSeeOther) 
    }
  }
}

func logout(w http.ResponseWriter, r *http.Request) {
  session, _ := store.Get(r, "lorymail")
  session.Values["loryname"] = nil //Necessary?
  session.Values["uid"] = nil
  session.Options = &sessions.Options{MaxAge: -1} //Deletes the session
  session.Save(r, w)
  http.Redirect(w, r, "/", http.StatusSeeOther)
}

func retr(w http.ResponseWriter, r *http.Request) {
  if r.Method == "POST" {
    session, _ := store.Get(r, "lorymail")
    if session.Values["loryname"] == nil || session.Values["uid"] == nil {
      http.Redirect(w, r, "/login", http.StatusSeeOther)
    }
    uid, ok := session.Values["uid"].(int)
    if !ok {
       return
    }
    
    body, _ := ioutil.ReadAll(r.Body)
    fmt.Println(string(body))
    var filter Filter
    json.Unmarshal(body, &filter)
    
    user := user.UserMap[uid]
  
    var letters []string
    
    chunk := filter.Chunk
    
    //TODO: refactor
    if len(filter.Terms) > 0 {
      indexSet := user.SearchMail(filter.Terms)
      for i := range(indexSet) {
        email := user.Emails[i]
        letters = append(letters, utils.MakeLetter(email.Path))
      }
    } else {
      for i := (chunk-1)*200; i < len(user.AllSortedEmailList); i++ {
        email := user.Emails[user.AllSortedEmailList[i]]
        letters = append(letters, utils.MakeLetter(email.Path))
        
        if (i+1) % 200 == 0 {
          break
        }
      }
      fmt.Println(len(letters))
    }
    
    letterbag := "{ \"bag\": %s }"
    if len(letters) > 0 {    
      b, _ := json.Marshal(letters)
      letterbag = fmt.Sprintf(letterbag, string(b))
    } else {
      letterbag = fmt.Sprintf(letterbag, "[]")
    }
    fmt.Fprint(w, letterbag)
  }
}

func main() {
	http.HandleFunc("/", mailroom)
  http.HandleFunc("/login", login)
  http.HandleFunc("/logout", logout)
  http.HandleFunc("/retr", retr)
  //NOTE: will serve static files using Nginx, but for now use Go's FileServer
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
  http.ListenAndServe(":3000", nil)
}
