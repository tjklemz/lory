package email

import (
  "sort"
)

//-------- Types --------

type Email struct {
  Id int
  Path string
}

type EmailList []*Email

//-----------------------

func (e EmailList) Len() int {
  return len(e)
}

func (e EmailList) Swap(i, j int) {
  e[i], e[j] = e[j], e[i] 
}

func (e EmailList) Less(i, j int) bool {
  return e[i].Path < e[j].Path
}

//NOTE: should take an EmailMap type
func SortMail(emails map[int]*Email) []int {
  var eList EmailList
  for id := range(emails) {
    eList = append(eList, emails[id])
  }
  
  sort.Sort(eList)
  
  var sortedEmailList []int
  for i := range(eList) {
    sortedEmailList = append(sortedEmailList, eList[i].Id)
  }
  return sortedEmailList
}
