package utils

import (
  "fmt"
  "bytes"
  "io/ioutil"
  "net/mail"  
)

type IndexSet map[int]bool
type SearchMap map[string]IndexSet

func And(iSet IndexSet, other IndexSet) IndexSet {
  results := make(IndexSet)
  
  for k := range(other) {
    if _,b := iSet[k]; b {
      results[k] = true
    }
  }
  return results
}

func MakeLetter(filename string) string {
  //TODO: lazy loading here (like memcache)
  msg, _ := OpenLetter(filename)
  //fmt.Println(string(msg.Body))
  letter := fmt.Sprintf("%v - %v", msg.Header.Get("From"), msg.Header.Get("Subject"))
  return letter
}

func OpenLetter(filename string) (msg *mail.Message, err error) {
  b, err := ioutil.ReadFile(filename)
  if err != nil {
    panic(err)
  }
  reader := bytes.NewReader(b)
  msg, err = mail.ReadMessage(reader)
  return msg, err
}