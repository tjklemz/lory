#!/usr/bin/env ruby

if ARGV.length != 2 then
  puts "Incorrect usage. Try this:"
  puts
  puts "\truby #{__FILE__} <user> <num_emails>"
  puts
  exit(1)
end

user = ARGV[0]
num_emails = ARGV[1].to_i
  
if !Dir.exists? user then
  print "User folder '#{user}' doesn't exist.\nCreate it? (y/n) "
  ans = STDIN.gets.chomp.downcase
  if ans != 'y' then
    puts "\nDidn't create mails for user '#{user}'"
    exit(0)
  end
  Dir.mkdir(user, 0700)
end

num_emails.times do |i|
  to = "The Dude"
  
  message = <<MESSAGE_END
From: Thomas Klemz <tjklemz@example.com>
To: #{to} <guy@example.com>
Subject: Yoyo #{user} this be a test #{i+1}

Ironstone sigsbee adaptiveness unoffered dens phenazine fullness slopwork excitative herborizing unsaluting unroot excluder. Manistique kazan echoing spurrite noncongruous amphictyonic xenon cryogenics lawlessly supercommercial wctu ostentatiousness superhero violescent. Unmusing sudor haemus substation preexplanatory montoursville prebelieving lysing bazookaman reestablish abridgement poaceous iarovized pos. Demanding neurosis telesis portless gelett misery padre rhodesoid paran endlessly congenerous hemotoxic kohinur renotification. Pilgrimaging verificatory crossable van molly invade underadjustment hoactzin suspire encrinite resemblingly depict convive trenchard. Yamashita yahwistic hindus overhonesty overmoist kleptomaniac ascospore impulsion skive pledgor biogeny quaich consumerism epiplexis. Evolutionism lindsay overneutralizer bragg curlicue fluidize warrantableness spearfish rapid secular preciosity abiu diplostemony attuned.

Eridu pupilless weber blarney ananthous unprohibited mousily radarman bonnet centering matrimonial detoxicant subconjunctive aecidium. Unimprovised amok gushingly leviticus nitrolic abakan unclearable septuagesima invidious preequip antireligionist unfavouring palaeobotany tinsel. Noninfecting confessor sulphurated encephalitic nonambiguity folksy pythias sulphathiazole amated overword nazir nikolayev beaverette demobbing. Hilarious scumble satyrid adolescently pulleyless misgraded split amphioxi espy hepatotoxin euphorbia aepytus chicaned peterborough. Homeopath restiveness esb henty moonflower frustrative bandjermasin infantry farmerette deadly jehan appeasing hypotyposis preholder. Hallucinate damages dieselization tasset unbenumbed pandora chequer diabolism bequeathable fiendlike preluded sea maurist tachymetry. Berber wattless meissen glamorize wark cerous gratulated malie fleecier filagreeing noncommemoration photoreconnaissance climax udder.

Supersensualism screwed milanov groningen misalliance noncircumscribed replowing subarctic underdevelopment preexpounder rouault ventriculogram soaringly pantomimed. Endogamy liftoff polychasial nonconcentricity supercycle dolmetsch precambrian aristotelian szechwan outgazed perseveringly sarsen deferrable spokewise. Intercolonization secludedness etymologically parting fibrin bookstack qld callidice redictate seminasality resituated gossipingly complimentable archdeaconate. Thalamotomy hardie altissimo taurine unlent urinogenital achar superalkalinity unpromulgated erceldoune foolhardy tyrant honey charlatanism. Popocatepetl opening religiosity zakat cleaver prethyroid cardiganshire berchtesgaden expectance overhandicapped hamulus yaws lint inshore. Interentangle tallyman nonstyptical ibex laxativeness experimentative miseno cerebritis efficiency necrology halicarnassean blockheadedness unwisely mancunian. Unspirited motion untortious wrinkleless profuseness metaphysicize myxovirus plodder intimated unclaimed bioecology subhumid conditioned bolger.

Dominantly gritless uncavernous nonvalued barquentine importation bunn unmaintainable dumdum ambrosiaceous bkg e''er adermine manasquan. Azine aquavit tense distantly invading abator unfossilized tressier disserve undenuded intermitter vizorless unbrave yesternight. Tenement cont isotopy falkirk anaptyxis rummaged preoppression puerperium woodwaxen paceway wrinkliest antiaggression prelims immunologically. Phonendoscope phosphoprotein strife effluent syllogize keijo yeast investigator intervariation habituated underpopulation golding nonferocious strung. Orthopterous unexponible unpulleyed lumined exhumed orchil freedwomen canaletto orozco katchina psychologically carnal mli repossess. Segar unipetalous premarry reinscribing conclusionally curdy fleuret hopelessly vizagapatam oscillometry diurnal upland charleton pluralize. Resegregating thursday bombload behan forensic crocoisite theirselves dieselizing preformationary recommendable ataraxic earbash invagination strutting.

Peavey topologist dieselized barefacedness hyperesthesia metricised protax appenzell catchup ferried cerite plonk greeneries divan. Stigmatic unconnotative lancepod contagiousness stimulated sabbathless precipitation columbeion inimicable firethorn cantonal nonsibilant carrousel schizocarpic. Simionato numerology blouson crampon expatriation ives finned grog disepalous jeopardy milo diastolic borax esparto. Prespinal louche ecphonesis indeciduous muso indented underspecify diabolized cytopathology suffused southerliness unloved eudist outdoor. Dipterous glottic venialness allodia trunkful luge lanternfishes confervalike uninfuriated superannuity schizomycetic sparklessly flightiness overgilded. Unlexicographical unfulfillable trull flanker ceremony aliped nondenunciating jewellike reproachableness myrmecologist surinam undeeded reconcile unacted. Spottable plashier semel farcy wielder bovaristic stylohyoideus preinflict ardennes mizoram eleemosynary myrtilus guericke interresistance.

Boorishly terah presumptively hexapody jarry nos lipography hasanlu pahang playacting asthmatical absolutist prehostile preadmonish. Superingenious reversioner harmoniser lineated plugboard jezabel britannia hobble walies undexterous carmel uncourtesy antlia siphonal. Grumpier downily abortus avidity overpass cortes unmad precorruptive magnetohydrodynamics watchword babette shockstall incog undisappointed. Monogenistic firefang spineless alkalizer polyhistor apostil idoneity nonoriginal zwingli aeolic nonerection junket amphoric preconjectured. Absolute uncovetous uninhibiting honeying cowbird cockleboat epigrammatist nonprobation rickrack peasecod verdin blas semihumanistic proromanticism. Caterwaul palpated adnominal swigger flamer nonlubricating yellowish schoolboy decembrist priggish fieriness grist trisoctahedron doublure. Calvinist spadelike lamed noninhibitive para charleroi pseudogeneric rural vistula permalloy larkspur overgoad unfencing radioelement.

Swinishness whoredom ensheath kissimmee interjudgment recollectedly complected unexpedient opaque svetlana restate ishmael physiotherapist arachnida. Misaddressing tergum overimpressible tetchily calory unfailingness unbenevolence obliquity premonumental verbalist ernest inviolately macroclimate been. Caiaphas dustup unscarce enterer pomona lamely oilcup sleepiest intentionless predatory nonmalignant vassalage glebe ultrasonically. Nonpedigreed pederasty peperoni citharist resalt diamagnet tyrr dowager electrobiologically nephrism paneled retouchtracing monk electrovalence. Palankeeningly blackamoor euhemerus managerial chewink beiderbecke conspirator ungraphable loach alphabet dexamenus townscape epicardiac spenserian. Cowherb buttoner hucklebone skillion subsynodal mustang nonrecognition royalistic inswing abducentes cornish dukhobors superordination. Pekin obe colosseum soak disenthral pine artois ussr reinquiries cretinism autopsic eldo recost presupplicated.

Nonexplanatory airing extenuated ensigncy hatchet cynicism craziness ina clementine undisclaimed raf milkwood miriest slinkingly. Reaccede pandean nonsensorial rehonor hermit relevied antagonist crooksville jejunectomy unappendaged kunstlied mobilise nonyielding brassily. Thess nostology decagon cipherable covalently explainer militantness indianian unembossed sausage adorably decapitator woodenware stella. Apyretic fewness holler micrococcic thearchic unduplicative yeti euchologia judgement inspired subcrepitation recopying femora noninternational. Charlie interlying skep equator hooka matellasse berlon bricklaying agatha syncopated tubuphone greetingly dividableness proreduction. Prediminution headscarf formulism underpainting ayntab mridiennes delhi abient achromatise dom allen conceded radiology leavening. Tithable premenacing jacteleg implacable landwaiter imposingness precostal colloquialness pennie vigo dotting ebulliency incantation.

Overdiscourage fiddling vinylated lacunose truantly madrigalian reptiloid pest extrajudicially hellertown noritic musette developmental caroluses. Rufescence muenster oinochoai dysuric condoler dniester neuroembryological antireform comfortless unthroatily lewisham biggie parterre rabbinic. Betti nonbourgeois consuming impregnation arbitrage kean hyposensitize coalface aphareus quotable imminency viscometric grindle mutilation. Noncellulous bloomed superhumeral neutrally conation dandier ooh photocopier ginzo selaginella gloomiest flawiest lenity oken. Trailboard detent quarrellingly profederation thrymsa coelostat untolerated blunging mourningly contemporised apicultural embargo aquarist potencaries. Orbicularis disciplelike corollate oxymoron overshaded osorno peculate unsufficing outdating samp adana collision nonneutral provostship. Correlated unapprehensible sceptre unstimulable monocline coolingness habakkuk cassite rhinoceros archetypally uncapitalistic fletchings unchosen purpleheart.

Temperateness smutting cardholder inhibitor pulsatility unfenced baccy reemploy unlooked pansil bpc ottoman dentilabial precreditor. Lustered microcrystalline aspired merovingian revegetating neuss brunhilda uncorrected misclassification coprophagous centrad emprise audiometer manipur. Expand deuterogamist inadvisably rhyolitic speckedness adventist coaxingly reschedule mansart inburst tiresias redintegrate summerliness narcoticness. Unconcrete disburthen magical centaurial intracutaneous preterite faberg ploeti thalweg regard prytaneum contractually monoploid vitreous. Holystoning ochery ratel inaptness beastliness orontius unwhelped nongrain seth bimaculate pettitoes autocatalysis unsick margarite. Wyclif nasolachrymal regarrison postpharyngeal pedasus ophthalmoscopist phototropism nonretardative ectasis inventorial zygoma ardene inculpation embrocated. Uncircuitous gigawatt desirable inscription happiest barbarianism traceably sterical pseudorepublican ossiferous
MESSAGE_END
  
  File.write("#{user}/myemail#{i+1}.txt", message)
end
